imapmemo 
========

### Goals ###
Store a memo on a IMAP server, iCloud style. 

### Steps ###
The application requires a systray zone. 
At startup, the application loads the local file.
Upon modification, when the user leaves he is asked 
whether to save the file or not (if he did not 
already save).
The file is then uploaded to the IMAP server (in 
the "Notes" folder). 
It is then available on another device. 

### Build ###
Tested on Linux with stalonetray. 