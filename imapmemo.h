
#ifndef __IMAPMEMO_H__
#define __IMAPMEMO_H__

#include <QSystemTrayIcon>

#ifndef QT_NO_SYSTEMTRAYICON

#include <QDialog>

#include "imapclient.h"

class QAction;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QSpinBox;
class QTextEdit;
class QVBoxLayout;
class QIcon;

class imapmemo : public QDialog
{
    Q_OBJECT

public:
    imapmemo();

    void setVisible(bool visible) override;

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void showMessage();
    void messageClicked();

private:
    void createActions();
    void createTrayIcon();

    QLabel *iconLabel;
    QCheckBox *showIconCheckBox;

    QGroupBox *messageGroupBox;
    QLabel *typeLabel;
    QLabel *durationLabel;
    QLabel *durationWarningLabel;
    QLabel *titleLabel;
    QLabel *bodyLabel;
    QComboBox *typeComboBox;
    QSpinBox *durationSpinBox;
    QLineEdit *titleEdit;
    QTextEdit *editor;
    QVBoxLayout * layout;
    QPushButton *showMessageButton;

    QAction *minimizeAction;
    QAction *maximizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    QIcon * icon;
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

    imapclient * ic;
};

#endif // QT_NO_SYSTEMTRAYICON

#endif
