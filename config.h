#ifndef __CONFIG_H__
#define __CONFIG_H__

#define CONFIG_IMAP_SERVER  ("imap.server.net")
#define CONFIG_IMAP_PORT    (993)
#define CONFIG_LOGIN        ("username")
#define CONFIG_PASSWD       ("password")

#define CONFIG_FOLDER       ("Notes")

#define CONFIG_MIME_FROM    ("sender@server.net")
#define CONFIG_MIME_TO      ("dest@server.net")
#define CONFIG_MIME_DOMAIN  ("domain.net")

#endif
