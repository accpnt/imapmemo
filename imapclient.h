#ifndef __IMAPCLIENT_H__
#define __IMAPCLIENT_H__

#include <QtWidgets/QWidget>
#include <QSslSocket>

class imapclient : public QObject
{
	Q_OBJECT

public:
	imapclient(const QString & host, int port);
    void create_note(QString title, QString text);

signals:

private slots:
	void wait_for_greeting();
	void read_response();

private:
	QSslSocket * socket;

    void config();
    void write_command(QString cmd);
};


#endif
