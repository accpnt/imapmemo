#-------------------------------------------------
#
# Project created by QtCreator 2010-12-20T14:47:37
#
#-------------------------------------------------

QT       += core gui widgets network

CONFIG += qt debug

TARGET = imapmemo
TEMPLATE = app


SOURCES += main.cpp \
           imapmemo.cpp \
    imapclient.cpp

HEADERS  += imapmemo.h \
    config.h \
    imapclient.h

FORMS    += 

RESOURCES = imapmemo.qrc

LIBS += 
