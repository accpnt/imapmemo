#include <QtWidgets>
#include <QFile>

#include "imapmemo.h"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(imapmemo);

    QApplication a(argc, argv);
    
    imapmemo im;

    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system."));
        return 1;
    }

    QApplication::setQuitOnLastWindowClosed(false);

    im.show();

    return a.exec();
}
