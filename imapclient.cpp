#include <unistd.h>
#include <QMimeType>
#include <QUuid>
#include <QDateTime>

#include "imapclient.h"
#include "config.h"

static QString formatDateTimeWithTimeZoneAtEnd(const QDateTime &now, const QString &format);
static QString dateTimeToInternalDate(const QDateTime &now);
static QString dateTimeToRfc2822(const QDateTime &now);

imapclient::imapclient(const QString & host, int port)
{
	socket = new QSslSocket();

	connect(socket, SIGNAL(connected()), SLOT(wait_for_greeting()));
	connect(socket, SIGNAL(readyRead()), SLOT(read_response()));
	connect(socket, SIGNAL(connectionClosed()), this, SLOT(quit()));
	connect(socket, SIGNAL(delayedCloseFinished()), this, SLOT(quit()));

	socket->connectToHost(host, port);

    if (!socket)
	{
		socket = new QSslSocket(this);

		connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
			this, SLOT(socketStateChanged(QAbstractSocket::SocketState)));
		connect(socket, SIGNAL(encrypted()),
			this, SLOT(socketEncrypted()));
		connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(socketError(QAbstractSocket::SocketError)));
		connect(socket, SIGNAL(sslErrors(QList<QSslError>)),
			this, SLOT(sslErrors(QList<QSslError>)));
		connect(socket, SIGNAL(readyRead()),
			this, SLOT(socketReadyRead()));
	}

	socket->connectToHostEncrypted(host, port);
}

void imapclient::wait_for_greeting()
{
    qDebug("Connected; now waiting for the greeting");

    // Start configuring
    this->config();
}

void imapclient::write_command(QString cmd)
{
    qDebug() << CONFIG_IMAP_SERVER << " << " << cmd;

    socket->write(cmd.toUtf8(), cmd.length());
    socket->waitForReadyRead();
}

void imapclient::read_response()
{
	if(socket->canReadLine()) 
	{
        qDebug() << CONFIG_IMAP_SERVER << " >> " << QString::fromUtf8(socket->readAll());
	}
}

void imapclient::config()
{
    QString cmd;

    // Capability
    cmd.clear();
    cmd = QString("a001 CAPABILITY\r\n");
    write_command(cmd);

    // STARTTLS
    cmd.clear();
    cmd = QString("a001 STARTTLS\r\n");
    write_command(cmd);

    // Server authentication
    cmd.clear();
    cmd = QString("a001 LOGIN %1 %2\r\n").arg(CONFIG_LOGIN).arg(CONFIG_PASSWD);
    write_command(cmd);

    // Go to Notes folder
    cmd.clear();
    cmd = QString("a002 SELECT %1\r\n").arg(CONFIG_FOLDER);
    write_command(cmd);

    // Nap Time ;)
    //sleep(3);
    //socket.readAll();

    // Logging out, closing socket
    //socket->write("a004 LOGOUT");
    //socket->disconnectFromHost();
    //socket->waitForDisconnected(5000);

//error:
//    qDebug() << "Closing socket";
//    socket->close();
}

static QString formatDateTimeWithTimeZoneAtEnd(const QDateTime &now, const QString &format)
{
    // At first, try to find out the offset of the current timezone. That's the part which sucks.
    // If there's a more Qt-ish way of doing that, please let me know.
    // that's right, both of these command are actually needed
    QDateTime nowUtc = now.toUTC();
    nowUtc.setTimeSpec(Qt::LocalTime);

    // Got to cast to a signed type to prevent unsigned underflow here. Also go to 64bits because otherwise there'd
    // a problem when the value is out-of-range for an int32.
    int minutesDifference = (static_cast<qint64>(now.toTime_t()) - static_cast<qint64>(nowUtc.toTime_t())) / 60;
    int tzOffsetHours = qAbs(minutesDifference) / 60;
    int tzOffsetMinutes = qAbs(minutesDifference) % 60;
    // The rest is just a piece of cake now

    return QLocale(QStringLiteral("C")).toString(now, format) +
            QLatin1String(minutesDifference >= 0 ? "+" : "-") +
            QString::number(tzOffsetHours).rightJustified(2, QLatin1Char('0')) +
            QString::number(tzOffsetMinutes).rightJustified(2, QLatin1Char('0'));
}

/** @short Return current date in the RFC2822 format */
static QString dateTimeToRfc2822(const QDateTime &now)
{
    return formatDateTimeWithTimeZoneAtEnd(now, QStringLiteral("ddd, dd MMM yyyy hh:mm:ss "));
}

/** @short Return current date in the RFC3501's INTERNALDATE format */
static QString dateTimeToInternalDate(const QDateTime &now)
{
    return formatDateTimeWithTimeZoneAtEnd(now, QStringLiteral("dd-MMM-yyyy hh:mm:ss "));
}

void imapclient::create_note(QString title, QString text)
{
    QByteArray msg;
    QByteArray header;
    QByteArray cmd;
    QString html_body;
    QDateTime datetime(QDateTime::currentDateTime());;

    QUuid x_uuid = QUuid::createUuid();
    QUuid message_uuid = QUuid::createUuid();

    header.append("Content-Type: text/html;\r\n\tcharset=utf-8\r\n");
    header.append(QString("Subject: %1\r\n").arg(title));
    header.append(QString("From: %1\r\n").arg(CONFIG_MIME_FROM));
    header.append(QString("To: %1\r\n").arg(CONFIG_MIME_TO));
    header.append("X-Uniform-Type-Identifier : com.apple.mail-note\r\n");
    header.append(QString("Date: %1\r\n").arg(dateTimeToInternalDate(datetime)));
    header.append("Content-Transfer-Encoding: 7bit\r\n");
    header.append("Mime-Version : 1.0\r\n");
    header.append("X-Mail-Created-Date: Sun, 23 Jul 2017 21:58:42 +0200\r\n");
    header.append(QString("X-Universally-Unique-Identifier: %1\r\n").arg(x_uuid.toString().mid(1,36)));
    header.append(QString("Message-Id : <%1@%2>\r\n").arg(message_uuid.toString().mid(1,36)).arg(CONFIG_MIME_DOMAIN));
    header.append("Content-Transfer-Encoding: quoted-printable");

    // Notes have the subject in the first line of the body (yuck).
    // Furthermore, if this line is later edited in the Note, then
    // the title of the note changes accordingly.
    html_body.append(QString("\r\n\r\n<div>%1</div>\r\n").arg(text));
    QString html = html_body.toHtmlEscaped();
    header.append(html_body.toUtf8());

    cmd.append("A003 APPEND " + QByteArray::fromStdString(CONFIG_FOLDER) + " {"  + QByteArray::number(header.size()) + "}\r\n");

    qDebug() << cmd;

    write_command(cmd);

    qDebug() << "Sending message";

    // We need this extra \n so that the IMAP server processes the literal
    header.append("\n");
    write_command(header);
}
